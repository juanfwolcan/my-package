# CHANGELOG



## v0.4.0 (2024-04-06)

### Feature

* feat(my_package): modificamos pipeline para publicar \n  BREAKING CHANGE ([`3262794`](https://gitlab.com/juanfwolcan/my-package/-/commit/3262794984dca5e5abddda6299e9c52d959fcf2f))


## v0.3.0 (2024-04-06)

### Feature

* feat(my_package): PAQUETIZAMOS EL MODULO BREAKING CHANGE ([`c72643d`](https://gitlab.com/juanfwolcan/my-package/-/commit/c72643d6bc2104f997a4d6c9d148339e63127210))


## v0.2.0 (2024-04-06)

### Feature

* feat(test): add new test module ([`dcdd813`](https://gitlab.com/juanfwolcan/my-package/-/commit/dcdd8139cf839b60666ea38aa8ad70cbdf44d072))


## v0.1.0 (2024-04-06)

### Feature

* feat(semantic-release): add semantic release to the repository ([`bc99ac4`](https://gitlab.com/juanfwolcan/my-package/-/commit/bc99ac4247c91de90f08b2269be219858f2c3b45))

### Unknown

* Volvemos al pipeline original ([`9289efd`](https://gitlab.com/juanfwolcan/my-package/-/commit/9289efd89adfb4e01977f0d86838d6fb73b4c659))


## v0.0.0 (2024-04-06)

### Unknown

* Modificamos la pipeline de deployment ([`a65c955`](https://gitlab.com/juanfwolcan/my-package/-/commit/a65c955eaf86d80d5120342e46aadddce966839b))

* Incorporamos en pyproject.toml la configuracion de semantic_release remoto ([`925751d`](https://gitlab.com/juanfwolcan/my-package/-/commit/925751df27d79578a64a143160db7dde9edd95fe))

* Inicializamos poetry con python version ^3.8 ([`e8a7281`](https://gitlab.com/juanfwolcan/my-package/-/commit/e8a728163424bec2977792a3f60791e4ae468f4f))

* Update .gitlab-ci.yml file ([`055c828`](https://gitlab.com/juanfwolcan/my-package/-/commit/055c828b71f049ab0b8a445f1a3442679c8ffddf))

* Add new file ([`e36ee0a`](https://gitlab.com/juanfwolcan/my-package/-/commit/e36ee0a8c1ab5d8b9430b13373346112f5410817))

* Initial commit ([`c502315`](https://gitlab.com/juanfwolcan/my-package/-/commit/c502315c419626cef952bbca682d01811f594140))
